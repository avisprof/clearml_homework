MLOps3. Spring 2024
==============================
## Project description

In this project we'll predict a full year worth of sales for various fictitious learning modules from different fictitious Kaggle-branded stores in different countries.

Dataset was taken from Kaggle:
https://www.kaggle.com/competitions/playground-series-s3e19

This dataset is completely synthetic, but contains many effects of real-world data, e.g., weekend and holiday effect, seasonality, etc.

The task of this project is to predict sales during for year 2022


## How to reproduce the project

Clone this repo into your local machine with the command:
`git clone https://gitlab.com/avisprof/clearml_homework.git`

Install dependencies from the `pytproject.toml` with the commands:
`pip install poetry`
`poetry install` 

Go to the folder [infra](/infra) and run `ClearML` locally with command:
`docker compose up -d`

After that go to your browser and go to the link:
`localhost:8080`

Enter your name, press `Start` button and then press `Create new credentials` button and copy credential information:
![clearml_credentials](reports/figures/01_clearml_credentials.png)

Run ClearML:
`clearml-init`

Paste configuration information:
![clearml_init](reports/figures/02_clearml_init.png)

## Prepare ClearML

Create new project:
`clearml-data create --project "Kaggle sales" --name "Raw data"`

Add data to project:
`clearml-data add --files data/raw/sales.zip`

Upload data to dataset:
`clearml-data upload`

Finish settings:
`clearml-data close`

![clearml_data](reports/figures/03_clearml_data.png)



## ClearML Exepriments

Run experiment with command:
`python src/clearml_experiment.py`

