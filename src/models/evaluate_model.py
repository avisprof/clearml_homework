import pickle
from pathlib import Path

import click
import pandas as pd
import json

from sklearn.metrics import mean_absolute_error, mean_squared_error

@click.command()
@click.argument("input_data", type=click.Path(exists=True))
@click.argument("input_model", type=click.Path(exists=True))
@click.argument("output_metric", type=click.Path())
def cli_evaluate_model(input_data: Path, input_model: Path, output_metric: Path):
    
    print(f"Read input file: {input_data}")
    data = pd.read_csv(input_data, index_col=0)

    print(f"Load model: {input_model}")
    with open(input_model, "rb") as f_in:
        model = pickle.load(f_in)

    results = evaluate(data, model)
   
    print(f"Save metrics to file: {output_metric}")
    with open(output_metric, "w") as f_out:
        json.dump(results, f_out, indent=2)

def evaluate(data, model):

    X = data.copy()
    y = X.pop('num_sold').values

    print(f"Make predictions ...")
    y_pred = model.predict(X)

    score_mae = mean_absolute_error(y, y_pred)
    score_rmse = mean_squared_error(y, y_pred, squared=False)
    model_name = model.__class__.__name__
    print(f"Evaluate {model_name}")
    print(f"MAE: {score_mae:.3f}")
    print(f"RMSE: {score_rmse:.3f}")

    return {"MAE": score_mae,
            "RMSE": score_rmse}

if __name__ == '__main__':
    cli_evaluate_model()

