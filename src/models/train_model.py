import pickle
from pathlib import Path

import click
import omegaconf
import pandas as pd

from sklearn.ensemble import RandomForestRegressor

@click.command()
@click.argument('input_file', type=click.Path(exists=True))
@click.argument("output_file_model", type=click.Path())
def cli_train_model(input_file: Path, output_file_model: Path):

    print(f'Read input file: {input_file}')
    data = pd.read_csv(input_file, index_col=0)

    model = train_model_rf(data)
    
    print(f'Save model to file: {output_file_model}')
    with open(output_file_model, 'wb') as f_out:
        pickle.dump(model, f_out)

def train_model_rf(data):

    cfg = omegaconf.OmegaConf.load('conf/config.yaml')
    params = cfg["models"]
    model_params = params["random_forest"]

    model = RandomForestRegressor(**model_params)
    model_name = model.__class__.__name__

    print(f"Train model {model_name} with params {model_params}")

    X = data.copy()
    y = X.pop('num_sold').values
    model.fit(X, y)

    return model



if __name__ == '__main__':
    cli_train_model()

