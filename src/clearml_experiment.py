import click
import joblib
import omegaconf

import pandas as pd
from clearml import Dataset, Task

from data.preprocess import preprocess_data
from data.split import split_data
from models.vectorize import train_vectorize
from features.encode import encode_data
from features.create_features import create_dates_features
from models.train_model import train_model_rf
from models.evaluate_model import evaluate

PROJECT_NAME = "Kaggle sales"
DATASET_NAME = "Raw data"


def cli_clearml():
    task = Task.init(project_name=PROJECT_NAME, task_name="baseline", output_uri=True)
    raw_data_path = Dataset.get(dataset_name=DATASET_NAME, dataset_project=PROJECT_NAME).get_local_copy()
    task.set_progress(0)

    print(f"Data path: {raw_data_path}")

    data = pd.read_csv(raw_data_path + "/sales.zip", index_col=0, parse_dates=['date'])
    print(f"Shape of raw data is {data.shape}")
    task.set_progress(10)

    data_clean = preprocess_data(data)
    task.set_progress(20)

    print(f"Shape of preprocess data is {data_clean.shape}")
    task.upload_artifact(name="processed_data", artifact_object=data_clean)

    data_train, data_test = split_data(data_clean)

    encoders = train_vectorize(data_train)
    joblib.dump(encoders, "models/vectorizer.pkl", compress=True)

    X_train = encode_data(data_train, encoders)
    X_train = create_dates_features(X_train)

    X_test = encode_data(data_test, encoders)
    X_test = create_dates_features(X_test)
    task.set_progress(50)

    task.upload_artifact(
        name="train_features",
        artifact_object=(X_train),
    )
    task.upload_artifact(
        name="test_features",
        artifact_object=(X_test),
    )

    model = train_model_rf(X_train)
    joblib.dump(model, "models/model.pickle", compress=True)
    task.set_progress(80)

    results = evaluate(X_test, model)

    task.set_progress(90)
    logger = task.get_logger()
    for metric, value in results.items():
        logger.report_single_value(metric, value)

    task.set_progress(100)


if __name__ == '__main__':
    cli_clearml()