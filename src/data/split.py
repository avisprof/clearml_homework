from pathlib import Path

import click
import omegaconf
import pandas as pd

from sklearn.model_selection import train_test_split

@click.command()
@click.argument('input_file', type=click.Path(exists=True))
@click.argument("output_file_train", type=click.Path())
@click.argument("output_file_test", type=click.Path())
def cli_split_data(input_file: Path, output_file_train: Path, output_file_test: Path):

    print(f'Read input file: {input_file}')
    data = pd.read_csv(input_file, index_col=0)

    data_train, data_test = split_data(data)

    print(f"Save data train to file: {output_file_train}")
    data_train.to_csv(output_file_train, compression='zip')

    print(f"Save data test to file: {output_file_test}")
    data_test.to_csv(output_file_test, compression='zip')

def split_data(data: pd.DataFrame):

    cfg = omegaconf.OmegaConf.load('conf/config.yaml')
    params = cfg["split"]

    print("Split data by train/test without shuffling...")
    data_train, data_test = train_test_split(data, 
                                             test_size=params["test_size"],
                                             shuffle=False)
    
    return data_train, data_test


if __name__ == '__main__':
    cli_split_data()
